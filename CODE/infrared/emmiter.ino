/**
 * Infrared - Emmiter
 *
 * Controla um emissor de sinais por infravermelho.
 *
 * Neste exemplo o emissor de sinais faz piscar o led de infravermelho a cada meio segundo.
 * NOTA: Este projecto necessita dois arduinos para funcionar correctamente. Este programa deve ser carregado apenas num dos arduinos.
 *
 * criado a 29 de Abril 2016
 * por Bruno Horta, André Rosa
 */

#define IRE 2

void setup() {
	/**
	 * Definir o modo de funcionamento do emissor
	 */
	pinMode(IRE, OUTPUT);
}

void loop() {
	/**
	 * Liga o LED infravermelho emitindo um sinal
	 */
	digitalWrite(IRE, HIGH);
	delay(500);
	/**
	 * Desliga o LED infravermelho
	 */
	digitalWrite(IRE, LOW);
	delay(500);
}