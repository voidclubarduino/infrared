/**
 * Infrared - Receiver
 *
 * Controla um receptor de sinais por infravermelho.
 *
 * Neste exemplo o receptor infravermelho fica à escuta que o emissor envie sinais. Sempre que for recebido um sinal
 * o LED indicará que um sinal foi recebido.
 * NOTA: Este projecto necessita dois arduinos para funcionar correctamente. Este programa deve ser carregado apenas num dos arduinos.
 *
 * criado a 29 de Abril 2016
 * por Bruno Horta, André Rosa
 */

#define IRD 8
#define LED_INDICATOR 13

void setup() {
  Serial.begin(9600);
  /**
   * Definir o modo de funcionamento do receptor
   */
  pinMode(IRD, INPUT);
  /**
   * Definir o modo de funcionamento do LED indicador
   */
  pinMode(LED_INDICATOR, OUTPUT);
}

void loop() {
  /**
   * Ler os dados do receptor
   */
  int detected = digitalRead(IRD);
  /**
   * Definir o estado do LED indicador
   */
  if (detected == 0) { digitalWrite(LED_INDICATOR, HIGH); }
  if (detected == 1) { digitalWrite(LED_INDICATOR, LOW); }
  Serial.println(detected);
}